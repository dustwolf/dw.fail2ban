# Ansible Fail2Ban

Ansible role for installing Fail2Ban protection.

Currently made to work with CentOS 8 stream Linux.

## Note

This probably needs some more work to make sure the stock jails actually work.

## Project status
This is a work in progress. I am using this to manage my own private servers and I will contribute as time allows.

You may use this code if you find it useful.
